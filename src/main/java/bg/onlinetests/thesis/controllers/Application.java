package bg.onlinetests.thesis.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import bg.onlinetests.thesis.business.TestBusiness;

@Controller
public class Application {

	@Autowired
	private TestBusiness testBusiness;
	
	@RequestMapping(value = "/index")
	String testAction(Map<String, Object> params, HttpServletRequest request) {
		String test = testBusiness.testBusiness("This is initial set up of", " my thesis");
		params.put("test", test);
		return "index";
	}
}
