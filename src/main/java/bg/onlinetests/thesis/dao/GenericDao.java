package bg.onlinetests.thesis.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import bg.onlinetests.thesis.models.Entity;

public class GenericDao<T extends Entity> {

	protected EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	
}
